# Events

Simple pub/sub

Fork for use with php version >=7.2.34

## Install

Via Composer

``` bash
$ composer require zeageorge/events_7234
```

## Usage

``` php
<?php

declare(strict_types=1);

use zeageorge\events_7234\Event;
use zeageorge\events_7234\EventHandlerInterface;

require 'vendor/autoload.php';

class AppStartedEventHandler implements EventHandlerInterface {
  public function handle(Event $event): void {
    echo "Application started at {$event->getData()}";
  }
}

$app_started_event = new Event('app_started');

$app_started_event->setData(date('Y-m-d'))->subscribe(new AppStartedEventHandler());

$app_started_event->publish();

```

