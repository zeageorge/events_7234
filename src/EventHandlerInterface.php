<?php

declare(strict_types=1);

namespace zeageorge\events_7234;

/**
 * Description of EventHandlerInterface
 *
 * @author Zeakis George <zeageorge@gmail.com>
 */
interface EventHandlerInterface {
  /**
   *
   * @param Event $event
   */
  public function handle(Event $event): void;
}
