<?php

declare(strict_types=1);

namespace zeageorge\events_7234;

/**
 * Description of EventInterface
 *
 * @author Zeakis George <zeageorge@gmail.com>
 */
interface EventInterface {
  const MAX_PRIORITY = 0;
  const DEFAULT_PRIORITY = 127;
  const MIN_PRIORITY = 255;

  /**
   *
   * @return self
   */
  public function publish(): self;

  /**
   * Subscribe to this event.
   *
   * @param EventHandlerInterface $handler the event handler
   * @param int $priority
   * @return self
   */
  public function subscribe(EventHandlerInterface $handler, int $priority = self::DEFAULT_PRIORITY): self;

  /**
   *
   * @param EventHandlerInterface $handler
   * @return self
   */
  public function unsubscribe(EventHandlerInterface $handler): self;

  /**
   * The name of the Event
   *
   * @return string
   */
  public function getName(): ?string;

  /**
   *
   * @return array An assoc array like:
   * [
   *   priority0=>[EventHandlerInterface, EventHandlerInterface, ...]
   *   priority1=>[EventHandlerInterface, EventHandlerInterface, ...]
   *   ...
   *   priorityN=>[EventHandlerInterface, EventHandlerInterface, ...]
   * ]
   */
  public function getHandlers(): array;

  /**
   *
   * @param int $priority
   * @return EventHandlerInterface[]
   */
  public function getHandlersByPriority(int $priority): array;

  /**
   *
   * @return object The object this Event belongs to
   */
  public function getParent(): ?object;

  /**
   *
   * @return boolean True if the Event is complete and no further listeners should be called. False to continue calling listeners.
   */
  public function isPropagationStopped(): bool;

  /**
   *
   * @return mixed any additional information
   */
  public function getData();

  /**
   *
   * @param bool $propagation
   * @return self
   */
  public function stopPropagation(bool $propagation): self;

  /**
   *
   * @param mix $data
   * @return self
   */
  public function setData($data): self;
}
