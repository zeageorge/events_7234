<?php

declare(strict_types=1);

namespace zeageorge\events_7234;

use function
  ksort,
  array_key_exists;

/**
 * Description of Event
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Event implements EventInterface {
  /** @var string The name of this Event */
  protected $name;

  /**
   * @var array eg.
   * [
   *   priority0 => [EventHandlerInterface, EventHandlerInterface, ...],
   *   priority1 => [EventHandlerInterface, EventHandlerInterface, ...],
   *   ...
   *   priorityN => [EventHandlerInterface, EventHandlerInterface, ...],
   * ]
   *
   */
  protected $handlers = [];

  /** @var object the parent of this event */
  protected $parent;

  /** @var bool */
  protected $stopPropagation = false;

  /** @var mixed payload */
  protected $data;

  /**
   *
   * @param string $name
   * @param object $parent The object this Event belongs to
   * @param mixed $data
   */
  public function __construct(string $name = null, object $parent = null, $data = null) {
    $this->name = $name;
    $this->parent = $parent;
    $this->data = $data;
  }

  /**
   *
   * @inheritdoc
   */
  public function publish(): EventInterface {
    if ($this->stopPropagation) {
      return $this;
    }

    foreach ($this->handlers as $priority => $handlers) {
      foreach ($handlers as $handler) {
        $handler->handle($this);

        if ($this->stopPropagation) {
          return $this;
        }
      }
    }

    return $this;
  }

  /**
   *
   * @inheritdoc
   */
  public function subscribe(EventHandlerInterface $handler, int $priority = self::DEFAULT_PRIORITY): EventInterface {
    $priority = $priority >= self::MAX_PRIORITY && $priority <= self::MIN_PRIORITY ? $priority : self::DEFAULT_PRIORITY;

    if (array_key_exists($priority, $this->handlers)) {
      // check if handler already exists
      foreach ($this->handlers[$priority] as $existing_handler) {
        if ($existing_handler == $handler) {
          return $this;
        }
      }
    } else {
      $this->handlers[$priority] = [];
      ksort($this->handlers);
    }

    $this->handlers[$priority][] = $handler;

    return $this;
  }

  /**
   *
   * @inheritdoc
   */
  public function unsubscribe(EventHandlerInterface $handler): EventInterface {
    foreach ($this->handlers as $priority => $handlers) {
      foreach ($handlers as $existing_handler) {
        if ($existing_handler == $handler) {
          unset($this->handlers[$priority][$existing_handler]);
        }
      }
    }

    return $this;
  }

  /**
   *
   * @inheritdoc
   */
  public function getName(): ?string {
    return $this->name;
  }

  /**
   *
   * @inheritdoc
   */
  public function getHandlers(): array {
    return $this->handlers;
  }

  /**
   *
   * @param int $priority
   * @return EventHandlerInterface[]
   */
  public function getHandlersByPriority(int $priority): array {
    return $this->handlers[$priority] ?? [];
  }

  /**
   *
   * @inheritdoc
   */
  public function getParent(): ?object {
    return $this->parent;
  }

  /**
   *
   * @inheritdoc
   */
  public function isPropagationStopped(): bool {
    return $this->stopPropagation;
  }

  /**
   *
   * @inheritdoc
   */
  public function getData() {
    return $this->data;
  }

  /**
   *
   * @inheritdoc
   */
  public function stopPropagation(bool $propagation): EventInterface {
    $this->stopPropagation = $propagation;
    return $this;
  }

  /**
   *
   * @inheritdoc
   */
  public function setData($data): EventInterface {
    $this->data = $data;
    return $this;
  }
}
